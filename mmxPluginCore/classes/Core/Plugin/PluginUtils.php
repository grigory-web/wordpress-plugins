<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 11.04.16
 * Time: 14:39
 */

namespace Core\Plugin;


trait PluginUtils
{
    protected function attachActionListener($actionId, $methodName, $priority = 10, $acceptedArgs = 1) {
        add_action($actionId, array($this, $methodName), $priority, $acceptedArgs);
    }

    protected function attachFilter($filterId, $methodName, $priority = 10, $args = 1) {
        add_filter($filterId, array($this, $methodName), $priority, $args);
    }

    protected function addShortcode($shortcode, $methodName) {
        add_shortcode($shortcode, array($this, $methodName));
    }
}