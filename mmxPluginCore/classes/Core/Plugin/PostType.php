<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 22.06.16
 * Time: 1:11
 */

namespace Core\Plugin;


abstract class PostType
{
    use PluginUtils;

    private $name;
    private $namePlural;

    private $slug;
    private $slugPlural;

    private $menuIcon;
    private $supports;
    private $hasArchive;

    private $customStatusPolicy;
    private $customStatuses;
    private $statusRenaming;
    private $statusRemoval;

    const STATUS_NONE = 0;
    const STATUS_APPEND = 1;
    const STATUS_ONLY = 2;

    public function __construct($slug, $slugPlural, $name, $namePlural, $menuIcon = null, array $supports = array(), $hasArchive = false, $customStatusPolicy = PostType::STATUS_NONE, $customStatuses = array(), $statusRenaming = array(), $statusRemoval = array())
    {
        $this->slug = $slug;
        $this->slugPlural = $slugPlural;
        $this->name = $name;
        $this->namePlural = $namePlural;
        $this->menuIcon = $menuIcon;
        $this->supports = $supports;
        $this->hasArchive = $hasArchive;

        $this->attachActionListener("init", "registerNewPostType");
        $this->attachActionListener("save_post", "savePostData", 10, 3);
        $this->attachActionListener('post_edit_form_tag', "embedMultipart");

        if ($customStatusPolicy != PostType::STATUS_NONE) {
            $this->customStatusPolicy = $customStatusPolicy;
            $this->customStatuses = $customStatuses;
            $this->statusRenaming = $statusRenaming;
            $this->statusRemoval = $statusRemoval;

            $this->attachActionListener("init", "registerNewPostStatuses");

            $this->attachFilter("views_edit-" . $slugPlural, "modifyPostStatuses");

            $postTypes = get_post_types();
            foreach ($postTypes as $postType) {
                $this->attachFilter("views_edit-" . $postType . "s", "removeForeignStatuses");
            }
        }
    }

    public final function removeForeignStatuses($statuses) {
        foreach ($this->customStatuses as $status => $unused) {
            unset($statuses[$status]);
        }

        return $statuses;
    }

    public final function modifyPostStatuses($statuses) {
        if ($this->customStatusPolicy == PostType::STATUS_ONLY) {
            $statusesArray = array_keys($this->customStatuses);
            $currentStatusesArray = array_keys($statuses);

            $toDelete = array_diff($currentStatusesArray, $statusesArray);

            foreach ($toDelete as $deleteKey) {
                unset($statuses[$deleteKey]);
            }
        }

        if (count($this->statusRenaming) > 0) {
            foreach ($this->statusRenaming as $status => $caption) {
                $count = substr($statuses[$status], strpos($statuses[$status], "("));

                $statuses[$status] = $caption . $count;
            }
        }

        foreach ($this->statusRemoval as $status) {
            unset($statuses[$status]);
        }

        return $statuses;
    }

    public final function embedMultipart() {
        echo ' enctype="multipart/form-data"';
    }

    public final function registerNewPostStatuses() {
        foreach ($this->customStatuses as $status => $args) {
            register_post_status($status, $args);
        }
    }

    public final function registerNewPostType() {
            $labels = array(
                'name'				=> $this->namePlural,
                'singular_name'		=> $this->name,
                'add_new'			=> __("Add New") . " " . $this->name,
                'add_new_item'		=> __("Add New") . " " . $this->name,
                'edit'				=> __("Edit") . " " . $this->name,
                'edit_item'			=> __("Edit") . " " . $this->name,
                'new_item'			=> __("New") . " " . $this->name,
                'view'				=> __("View") . " " . $this->name,
                'view_item'			=> __("View") . " " . $this->name,
                'search_items'		=> __("Search") . " " . $this->namePlural,
                'not_found'			=> __("Nothing found"),
                'not_found_in_trash'=> __("Nothing found in Trash"),
                'parent_item_colon'	=> '',
                'all_items' 		=>  __("All") . " " . $this->namePlural,
            );

            $args = array(
                'labels'				=> $labels,
                'public'				=> true,
                'publicly_queryable' 	=> true,
                'exclude_from_search'	=> true,
                'show_ui'				=> true,
                'hierarchical'			=> false,
                'rewrite'				=> array('slug' => $this->slug, 'with_front' => 'true'),
                'query_var'				=> true,
                'menu_position'			=> null,
                'menu_icon'				=> $this->menuIcon,
                'supports'				=> $this->supports,
                'has_archive'           => $this->hasArchive,
                'capability_type'		=> array($this->slug, $this->slugPlural),
                'map_meta_cap'          => false,
                'register_meta_box_cb'  => array($this, "registerPostTypeMetaboxes")
            );

            register_post_type($this->slugPlural, $args);
    }

    public function registerPostTypeMetaboxes(\WP_Post $post) {
        add_meta_box($this->slug . "_metabox", $this->name, array($this, "renderPostTypeMetabox"), $this->slugPlural, "normal", "high");
    }

    public function renderPostTypeMetabox() {
        wp_nonce_field(basename(__FILE__), 'cp_nonce');
    }

    public function savePostData($post_id, $post, $update)
    {
        // Check post type
        if ($post->post_type != $this->slug) {
            return;
        }

        // Admin area only
        if (!is_admin()) {
            return;
        }

        // Verify nonce
        /*if (!isset($_POST['cp_nonce']) || !wp_verify_nonce($_POST['cp_nonce'], basename(__FILE__))) {
            return;
        }*/

        // Check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        //check post revision
        if (wp_is_post_revision($post_id)) {
            return;
        }

        $data = $this->handleSavePostData($post_id, $post, $update);
        $data['ID'] = $post_id;

        wp_update_post($data);
    }
    public abstract function handleSavePostData($postId, \WP_Post $post, $update);
}