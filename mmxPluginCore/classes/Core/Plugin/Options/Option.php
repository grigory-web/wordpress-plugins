<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 10.04.16
 * Time: 18:44
 */

namespace Core\Plugin\Options;


use Core\Plugin\PluginFragment;

abstract class Option
{
    private $id;
    private $caption;
    private $description;
    private $default;

    /**
     * @var PluginFragment
     */
    private $fragment;

    /**
     * Option constructor.
     * @param $id
     * @param $caption
     */
    public function __construct(PluginFragment $fragment, $id, $caption, $default = false, $description = "")
    {
        $this->id = $id;
        $this->caption = $caption;
        $this->default = $default;
        $this->description = $description;
        $this->fragment = $fragment;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getValue() {
        return get_option($this->generateFullId(), $this->default);
    }

    public abstract function generateOptionContent();

    protected function generateFullId()
    {
        return implode("_", array(
            $this->fragment->getPlugin()->getSlugName(),
            $this->fragment->getSlugName(),
            $this->id
        ));
    }

    public function register()
    {
        add_settings_field(
            $this->generateFullId(),
            $this->caption,
            array($this, "generateOptionContent"),
            $this->fragment->getPlugin()->getSlugName(),
            $this->fragment->getSettingsSectionId()
        );

        register_setting($this->fragment->getPlugin()->getSlugName(), $this->generateFullId());
    }


}