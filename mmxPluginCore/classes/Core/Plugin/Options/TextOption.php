<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 10.04.16
 * Time: 18:56
 */

namespace Core\Plugin\Options;

use Core\Plugin\PluginFragment;

class TextOption extends Option
{
    /**
     * TextOption constructor.
     */
    public function __construct(PluginFragment $fragment, $id, $caption, $default = "", $description = "")
    {
        parent::__construct($fragment, $id, $caption, $default, $description);
    }

    public function generateOptionContent()
    {
        echo '<input name="' . $this->generateFullId() . '" id="' . $this->generateFullId() . '" value="' . $this->getValue() . '" /> ' . $this->getDescription();
    }


}