<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 10.04.16
 * Time: 19:37
 */

namespace Core\Plugin\Options;


use Core\Plugin\PluginFragment;

class DropdownOption extends Option
{
    /**
     * @var array Array format: value => text
     */
    private $options;

    /**
     * DropdownOption constructor.
     * @param array $options
     */
    public function __construct(PluginFragment $fragment, $id, $caption, array $options, $default = false, $description = "")
    {
        parent::__construct($fragment, $id, $caption, $default, $description);
        $this->options = $options;
    }

    public function generateOptionContent()
    {
        echo '<select name="' . $this->generateFullId() . '" id="' . $this->generateFullId() . '"> ';

        $selectedOption = $this->getValue();

        foreach ($this->options as $value => $text) {
            echo "<option ";

            if ($value == $selectedOption) {
                echo "selected ";
            }

            echo 'value="' . $value . '">' . $text . "</option>";
        }

        echo '</select>' . $this->getDescription();
    }
}