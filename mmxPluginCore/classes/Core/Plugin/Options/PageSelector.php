<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 12.04.16
 * Time: 10:50
 */

namespace Core\Plugin\Options;


use Core\Plugin\PluginFragment;

class PageSelector extends DropdownOption
{
    private static $pages = null;

    /**
     * PageSelector constructor.
     */
    public function __construct(PluginFragment $fragment, $id, $caption, $default = false, $description = "", array $pageFilter = array("post_type" => "page", "numberposts" => -1))
    {
        parent::__construct($fragment, $id, $caption, $this->getPages($pageFilter), $default, $description);
    }

    private function getPages(array $filter) {
        if (self::$pages == null) {
            self::$pages = array();
            $fetchedPosts = get_posts($filter);
            if ($fetchedPosts) {
                foreach ($fetchedPosts as $post) {
                    self::$pages[$post->ID] = $post->post_title;
                }
            }
        }

        return self::$pages;
    }
}