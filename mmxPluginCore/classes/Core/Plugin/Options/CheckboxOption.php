<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 10.04.16
 * Time: 19:04
 */

namespace Core\Plugin\Options;


use Core\Plugin\PluginFragment;

class CheckboxOption extends Option
{
    /**
     * CheckboxOption constructor.
     */
    public function __construct(PluginFragment $fragment, $id, $caption, $default = false, $description = "")
    {
        parent::__construct($fragment, $id, $caption, $default, $description);
    }

    public function generateOptionContent()
    {
        echo '<input type="checkbox" name="' . $this->generateFullId() . '" id="' . $this->generateFullId() . '"';
        if ($this->getValue()) {
            echo ' checked';
        }
        echo '/> ' . $this->getDescription();
    }


}