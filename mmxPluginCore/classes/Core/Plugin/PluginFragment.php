<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 11.04.16
 * Time: 14:44
 */

namespace Core\Plugin;


use Core\Plugin\Options\Option;
use Core\Plugin\Options\CheckboxOption;

abstract class PluginFragment
{
    use PluginUtils;

    /**
     * @var string Fragment's slug name
     */
    private $fragmentSlugName;

    /**
     * @var string Fragment's human-readable name
     */
    private $humanReadableName;

    /**
     * @var Option[] Options the fragment depends on. Mnemonic => Option
     */
    private $options = array();

    /** @var bool  */
    private $isDeactivatable;

    /**
     * @var PluginEntryPoint
     */
    private $plugin;

    /**
     * PluginFragment constructor.
     * @param string $fragmentSlugName
     */
    public function __construct(PluginEntryPoint $plugin, $fragmentSlugName, $humanReadableName, $isDeactivatable = true)
    {
        $this->fragmentSlugName = $fragmentSlugName;
        $this->humanReadableName = $humanReadableName;
        $this->plugin = $plugin;
        $this->isDeactivatable = $isDeactivatable;

        //Register options
        if ($isDeactivatable) {
            $this->registerOption("active", new CheckboxOption($this, "active", $this->humanReadableName . " enabled", true));
        }
        $this->registerOptions();
    }

    public function getOption($mnemonic) {
        return $this->options[$mnemonic];
    }

    public function isEnabled() {
        if ($this->isDeactivatable) {
            return $this->getOption("active")->getValue();
        } else {
            return true;
        }
    }

    public function isCustomizable() {
        return (count($this->options) > 0);
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function getSlugName()
    {
        return $this->fragmentSlugName;
    }

    public function getSettingsSectionId()
    {
        return implode("_", array(
            $this->plugin->getSlugName(),
            $this->fragmentSlugName,
            "section"
        ));
    }

    public function getHumanReadableName()
    {
        return $this->humanReadableName;
    }

    protected function registerOption($mnemonic, Option $option) {
        $this->options[$mnemonic] = $option;
    }

    protected abstract function registerOptions();

    public function getPlugin()
    {
        return $this->plugin;
    }

    public function registerSection() {
        add_settings_section(
            $this->getSettingsSectionId(),
            $this->humanReadableName,
            array($this, "onGenerateSection"),
            $this->plugin->getSlugName()
        );

        foreach ($this->options as $option) {
            $option->register();
        }
    }

    public function onGenerateSection() {

    }


}