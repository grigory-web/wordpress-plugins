<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 11.04.16
 * Time: 14:38
 */

namespace Core\Plugin;

abstract class PluginEntryPoint
{
    use PluginUtils;

    /**
     * @var string A slug name of the plugin
     */
    private $pluginSlugName;

    /**
     * @var string A human-readable name of the plugin
     */
    private $humanReadableName;

    /**
     * @var PluginFragment[] Plugin Fragments. mnemonic => fragment
     */
    private $pluginFragments = array();

    /**
     * PluginEntryPoint constructor.
     */
    protected function __construct($pluginSlugName, $humanReadableName)
    {
        $this->pluginSlugName = $pluginSlugName;
        $this->humanReadableName = $humanReadableName;

        //Attach plugin fragments
        $this->attachPluginFragments();

        //Check if there are any fragments
        if (count($this->pluginFragments) > 0) {
            //Check if there are any settings in the fragments
            foreach ($this->pluginFragments as $fragment) {
                if ($fragment->isCustomizable()) {
                    //Register settings
                    $this->attachActionListener("admin_menu", "createMenuEntry");
                    $this->attachActionListener("admin_init", "initSettings");
                    break;
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getSlugName()
    {
        return $this->pluginSlugName;
    }

    public function getFragment($mnemonic) {
        return $this->pluginFragments[$mnemonic];
    }



    protected final function attachPluginFragment($mnemonic, PluginFragment $fragment) {
        $this->pluginFragments[$mnemonic] = $fragment;
    }

    protected abstract function attachPluginFragments();

    public function generateSettingsPage() {
        ?>
        <h2><?php echo $this->humanReadableName; ?></h2>
        <form method="POST" action="options.php">
            <?php settings_fields($this->pluginSlugName);	//pass slug name of page, also referred
            //to in Settings API as option group name
            do_settings_sections($this->pluginSlugName); 	//pass slug name of page
            submit_button();
            ?>
        </form>
        <?php
    }

    public function createMenuEntry() {
        add_options_page(
            $this->humanReadableName . " Settings",
            $this->humanReadableName,
            "manage_options",
            $this->pluginSlugName,
            array($this, "generateSettingsPage")
        );
    }

    public function initSettings() {
        /*if (!$this->settingsPage)
            return;*/

        foreach ($this->pluginFragments as $fragment) {
            if ($fragment->isCustomizable()) {
                $fragment->registerSection();
            }
        }
    }
}