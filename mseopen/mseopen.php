<?php

/**
 * Plugin Name: MSE Open
 * Version: 1.0.0
 * Author: MT

 */



/**
 * register menu for MSE users
 * 
 */
function register_menu_mse() {
	register_nav_menu('mse-menu', __('MSE Users Menu'));
}
add_action('init', 'register_menu_mse');


 function reg_menu_mse(){
            if(current_user_can("msc_user")){
                add_filter( 'wp_nav_menu_items', 'mse_user_menu_items', 10, 2 );
        }
}

add_action('init', 'reg_menu_mse');


function mse_user_menu_items ( $items, $args ) {
     $locations = get_registered_nav_menus();
        $menu_name = 'mse-menu';
        $locations = get_nav_menu_locations();
        $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
        $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
        $current_user = wp_get_current_user();
    if ($args->theme_location == 'primary') {
        $items='';
        foreach( $menuitems as $item ):
	        // set up title and url
	        $title = $item->title;
	        $link = $item->url;
	        $items.='<li class="menu-item"><a href="'.$link.'" class="title">'.$title.'</a></li>';
	    endforeach;
	    global $mscPluginEntry;
		$msedit_link= $mscPluginEntry->obtainMscApiInstance()->getCurrentUserWrapped()->getEditProfileLink();
	    $items.='<li class="menu-account-div">
	    			<a href="'.$msedit_link.'" class="new-menu-link has-child">'.$current_user->user_login.'</a>
	    			<ul class="user-sub-menu" style="">';
	    			$showBalance = false;
	    			$showBalance = true;
	    			global $mscPluginEntry;
	    			$balance = $mscPluginEntry->getFragment("balance")->getUserBalance(get_current_user_id());
				if ($showBalance) {
					$items.='<li class="menu-item"><a href="#"><span>Balance:'.$balance . ' ' . get_woocommerce_currency().'</span></a></li>
					<li class="menu-item"><a href=""><span>Withdraw</span></a></li>';
				 }

			 $items.='<li id="menu-item-111471" class="menu-item"><a href="http://onlineevent.com.au/my-account/view-order/" itemprop="url"><span itemprop="name">Tickets</span></a></li>

			<li id="menu-item-111472" class="menu-item"><a href="http://onlineevent.com.au/events/" itemprop="url"><span itemprop="name">Events</span></a></li>
			<li id="menu-item-111473" class="menu-item"><a href="'.$msedit_link.'" itemprop="url"><span itemprop="name">Manage Profile </span></a></li>

			<li id="menu-item-111474" class="menu-item"><a href="http://onlineevent.com.au/contact/" itemprop="url"><span itemprop="name">Contact</span></a></li>

		</ul></li>
	    <li class="menu-account-div"><a href="' . wp_logout_url() . '" class="new-menu-link has-child">Logout</a></li>';
    }
    return $items;
}

function mse_user_info(){
	global $current_user;
	if(current_user_can("msc_user")){

	}
	
}

add_action('init', 'mse_user_info');