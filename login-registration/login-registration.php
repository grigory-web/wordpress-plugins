<?php
/*
Plugin Name: Login and Registration - Custom Form Fields
Plugin URI: http://vishalramani.in/
Description: Provides simple front end registration and login forms with custom fields.
Version: 1.0
Author: Vishal Ramani
Author URI: https://www.upwork.com/freelancers/~01a5ae2a11152f601a
*/

// user registration login form
function vr_registration_form() {
 
	// only show the registration form to non-logged-in members
	if(!is_user_logged_in()) {
 
		global $vr_load_css;
 
		// set this to true so the CSS is loaded
		$vr_load_css = true;
 
		// check to make sure user registration is enabled
		$registration_enabled = get_option('users_can_register');
 
		// only show the registration form if allowed
		if($registration_enabled) {
			$output = vr_registration_form_fields();
		} else {
			$output = __('User registration is not enabled');
		}
		return $output;
	}
}
add_shortcode('register_form', 'vr_registration_form');


// user login form
function vr_login_form() {
	die("Login");
 
	if(!is_user_logged_in()) {
 
		global $vr_load_css;
 
		// set this to true so the CSS is loaded
		$vr_load_css = true;
 
		$output = vr_login_form_fields();
	} else {
		// could show some logged in user info here
		// $output = 'user info here';
	}
	return $output;
}
add_shortcode('login_form', 'vr_login_form');

// registration form fields
function vr_registration_form_fields() {
 
	ob_start(); ?>
		<?php 
		// show any error messages after form submission
		vr_show_error_messages(); ?>
		<script src='https://www.google.com/recaptcha/api.js'></script>
		<div class="container register-area">
			<div class="one-sixth first">&nbsp;</div>
			<div class="four-sixths">
				<form id="vr_registration_form" class="vr_form" action="" method="POST">
					
					<div class="section-hr"></div>
					<div class="form-section">
						<div class="two-sixths first">
							<label><?php _e('your information'); ?><span class="required">*</span></label>
						</div>
						<div class="two-sixths">
							<input name="vr_user_first" id="vr_user_first" class="contact-field" type="text" placeholder="First Name" />
						</div>
						<div class="two-sixths">
							<input name="vr_user_last" id="vr_user_last" class="contact-field" type="text" placeholder="Last Name" />
						</div>
					</div>
					<div class="form-section">
						<div class="two-sixths first">&nbsp;</div>
						<div class="two-sixths">
							<input name="vr_user_login" id="vr_user_login" class="contact-field" class="required" type="text" placeholder="User Name" />
						</div>
						<div class="two-sixths">
							<input name="vr_user_phone" id="vr_user_phone" class="contact-field" type="text" placeholder="Phone Number" />
						</div>
					</div>
					<div class="form-section">
						<div class="two-sixths first">&nbsp;</div>
						<div class="four-sixths">
							<input name="vr_user_email" id="vr_user_email" class="contact-field" class="required" type="email" placeholder="Email Address" />
						</div>
					</div>
					<div class="section-hr"></div>
					<div class="form-section">
						<div class="two-sixths first">
							<label><?php _e('your password'); ?><span class="required">*</span></label>
						</div>
						<div class="two-sixths">
							<input name="vr_user_pass" id="password" class="contact-field" class="required" type="password" placeholder="Enter Password" />
						</div>
						<div class="two-sixths">
							<input name="vr_user_pass_confirm" id="password_again" class="contact-field" class="required" type="password" placeholder="Confirm Password" />
						</div>
					</div>
					<div class="section-hr"></div>
					<div class="form-section">
						<div class="two-sixths first">
							<label><?php _e('subscribe to our newsletter'); ?></label>
						</div>
						<div class="one-sixth">
							<label class="radio">
								<input name="vr_user_subscribe"  type="radio" value="1" checked/>
								<span class="outer"></span><span class="radio-text"><?php _e('Yes'); ?></span>
							</label>
						</div>
						<div class="one-sixth">
							<label class="radio">
								<input name="vr_user_subscribe" type="radio" value="0"/>
								<span class="outer"></span><span class="radio-text"><?php _e('No'); ?></span>
							</label>
						</div>
						<div class="two-sixths">&nbsp;</div>
					</div>
					<div class="section-hr"></div>
					<div class="form-section">
						<div class="two-sixths first">
							<label><?php _e('captcha'); ?><span class="required">*</span></label>
						</div>
						<div class="four-sixths">
							<div class="g-recaptcha" data-sitekey="6LdeCx8TAAAAAEY44lMmpC6pHQSf_pSrEWIGvUrP"></div>
						</div>
					</div>
					<div class="section-hr"></div>
					<div class="form-section">
						<div class="two-sixths first">
							<label><?php _e('terms of use'); ?><span class="required">*</span></label>
						</div>
						<div class="four-sixths register-input-padding">
							<input name="vr_user_terms" id="vr_terms" class="required register-checked" type="checkbox"/>
							<span class="register-text-agree"><?php _e('I have read and agree to the <a href="javascript:void(0);" class="action-link">Terms of Use</a>'); ?></span>
						</div>
					</div>
					<div class="section-hr"></div>
					<div class="form-section">
						<div class="two-sixths first">&nbsp;</div>
						<div class="two-sixths">
							<input type="hidden" name="vr_register_nonce" value="<?php echo wp_create_nonce('vr-register-nonce'); ?>"/>
							<input type="submit" class="btn-blue" value="<?php _e('Submit'); ?>"/>
						</div>
						<div class="two-sixths">&nbsp;</div>
					</div>
					
				</form>
				<script type="text/javascript">
					jQuery(document).ready(function(){
						jQuery('#vr_registration_form').submit(function(){
							if(grecaptcha.getResponse() == ""){
								alert("Please enter captcha");
							}
						});
						jQuery('.register-checked').click(function () {
							if(jQuery(this).prop('checked') == true){
								jQuery(this).addClass('register-checked-active');
								jQuery(this).val(1);
							} else {
								jQuery(this).removeClass('register-checked-active');
								jQuery(this).val(0);
							}
						});
					});
				</script>
			</div>
			<div class="one-sixth">&nbsp;</div>
		</div>
		
	<?php
	return ob_get_clean();
}



// login form fields
function vr_login_form_fields() {
 
	ob_start(); ?>
		<h3 class="vr_header"><?php _e('Login'); ?></h3>
 
		<?php
		// show any error messages after form submission
		vr_show_error_messages(); ?>
 
		<form id="vr_login_form"  class="vr_form"action="" method="post">
			<fieldset>
				<p>
					<label for="vr_user_Login">Username</label>
					<input name="vr_user_login" id="vr_user_login" class="required" type="text"/>
				</p>
				<p>
					<label for="vr_user_pass">Password</label>
					<input name="vr_user_pass" id="vr_user_pass" class="required" type="password"/>
				</p>
				<p>
					<input type="hidden" name="vr_login_nonce" value="<?php echo wp_create_nonce('vr-login-nonce'); ?>"/>
					<input id="vr_login_submit" type="submit" value="Login"/>
				</p>
			</fieldset>
		</form>
	<?php
	return ob_get_clean();
}

// logs a member in after submitting a form
function vr_login_member() {
 
	if(isset($_POST) && isset($_POST['vr_user_login']) && isset($_POST['vr_login_nonce']) && wp_verify_nonce($_POST['vr_login_nonce'], 'vr-login-nonce')) {
 
		// this returns the user ID and other info from the user name
		$user = get_userdatabylogin($_POST['vr_user_login']);
 
		if(!$user) {
			// if the user name doesn't exist
			vr_errors()->add('empty_username', __('Invalid username'));
		}
 
		if(!isset($_POST['vr_user_pass']) || $_POST['vr_user_pass'] == '') {
			// if no password was entered
			vr_errors()->add('empty_password', __('Please enter a password'));
		}
 
		// check the user's login with their password
		if(!wp_check_password($_POST['vr_user_pass'], $user->user_pass, $user->ID)) {
			// if the password is incorrect for the specified user
			vr_errors()->add('empty_password', __('Incorrect password'));
		}
 
		// retrieve all error messages
		$errors = vr_errors()->get_error_messages();
 
		// only log the user in if there are no errors
		if(empty($errors)) {
 
			wp_setcookie($_POST['vr_user_login'], $_POST['vr_user_pass'], true);
			wp_set_current_user($user->ID, $_POST['vr_user_login']);	
			do_action('wp_login', $_POST['vr_user_login']);
 
			wp_redirect(home_url()); exit;
		}
	}
}
add_action('init', 'vr_login_member');

// register a new user
function vr_add_new_member() {
  	if (isset( $_POST["vr_user_login"] ) && wp_verify_nonce($_POST['vr_register_nonce'], 'vr-register-nonce')) {
		$user_login		= $_POST["vr_user_login"];	
		$user_email		= $_POST["vr_user_email"];
		$user_first 	= $_POST["vr_user_first"];
		$user_last	 	= $_POST["vr_user_last"];
		$user_phone	 	= $_POST["vr_user_phone"];
		$user_subscribe	= $_POST["vr_user_subscribe"];
		$user_pass		= $_POST["vr_user_pass"];
		$pass_confirm 	= $_POST["vr_user_pass_confirm"];
		if(isset($_POST["vr_user_terms"])){
			$user_terms 	= $_POST["vr_user_terms"];
		} else {
			$user_terms 	= '';
		}
 
		// this is required for username checks
		require_once(ABSPATH . WPINC . '/registration.php');
 
		if(username_exists($user_login)) {
			// Username already registered
			vr_errors()->add('username_unavailable', __('Username already taken'));
		}
		if(!validate_username($user_login)) {
			// invalid username
			vr_errors()->add('username_invalid', __('Invalid username'));
		}
		if($user_login == '') {
			// empty username
			vr_errors()->add('username_empty', __('Please enter a username'));
		}
		if(!is_email($user_email)) {
			//invalid email
			vr_errors()->add('email_invalid', __('Invalid email'));
		}
		if($user_phone == '') {
			//Invalid Phone Number
			vr_errors()->add('phone_invalid', __('Invalid Phone Number'));
		}
		if($user_subscribe == '') {
			//invalid Subscription
			vr_errors()->add('email_subscribe', __('Please Select Subscription'));
		}
		if(email_exists($user_email)) {
			//Email address already registered
			vr_errors()->add('email_used', __('Email already registered'));
		}
		if($user_pass == '') {
			// passwords do not match
			vr_errors()->add('password_empty', __('Please enter a password'));
		}
		if($user_pass != $pass_confirm) {
			// passwords do not match
			vr_errors()->add('password_mismatch', __('Passwords do not match'));
		}
		if($user_terms == '' || $user_terms == 'off') {
			//Terms and condition check
			vr_errors()->add('terms_invalid', __('Please select Terms and Condition'));
		}
 
		$errors = vr_errors()->get_error_messages();
 
		// only create the user in if there are no errors
		if(empty($errors)) {
 
			$new_user_id = wp_insert_user(array(
					'user_login'		=> $user_login,
					'user_pass'	 		=> $user_pass,
					'user_email'		=> $user_email,
					'first_name'		=> $user_first,
					'last_name'			=> $user_last,
					'user_phone'		=> $user_phone,
					'user_subscribe'	=> $user_subscribe,
					'user_registered'	=> date('Y-m-d H:i:s'),
					'role'				=> 'subscriber'
				)
			);
			if($new_user_id) {
				// send an email to the admin alerting them of the registration
				wp_new_user_notification($new_user_id);
 
				// log the new user in
				wp_setcookie($user_login, $user_pass, true);
				wp_set_current_user($new_user_id, $user_login);	
				do_action('wp_login', $user_login);
 
				// send the newly created user to the home page after logging them in
				wp_redirect(home_url()); exit;
			}
 
		}
 
	}
}
add_action('init', 'vr_add_new_member');

// used for tracking error messages
function vr_errors(){
    static $wp_error; // Will hold global variable safely
    return isset($wp_error) ? $wp_error : ($wp_error = new WP_Error(null, null, null));
}

// displays error messages from form submissions
function vr_show_error_messages() {
	if($codes = vr_errors()->get_error_codes()) {
		echo '<div class="vr_errors">';
		    // Loop error codes and display errors
		   foreach($codes as $code){
		        $message = vr_errors()->get_error_message($code);
		        echo '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
		    }
		echo '</div>';
	}	
}

// register our form css
function vr_register_css() {
	wp_register_style('vr-form-css', plugin_dir_url( __FILE__ ) . '/css/forms.css');
}
add_action('init', 'vr_register_css');

// load our form css
function vr_print_css() {
	global $vr_load_css;
 
	// this variable is set to TRUE if the short code is used on a page/post
	if ( ! $vr_load_css )
		return; // this means that neither short code is present, so we get out of here
 
	wp_print_styles('vr-form-css');
}
add_action('wp_footer', 'vr_print_css');