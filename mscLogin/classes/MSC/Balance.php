<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 03.07.16
 * Time: 22:40
 */

namespace MSC;


use Core\Plugin\Options\PageSelector;
use Core\Plugin\Options\TextOption;
use Core\Plugin\PluginFragment;

class Balance extends PluginFragment
{
    /**
     * Balance constructor.
     */
    public function __construct(\MSCLogin $entry)
    {
        parent::__construct($entry, "balance", "Balance");
        $this->attachActionListener("edit_user_profile", "embedBalanceEditor");
        $this->attachActionListener("show_user_profile", "embedBalanceEditor");
        $this->attachActionListener("edit_user_profile_update", "saveUserBalance");
        $this->attachActionListener("personal_options_update", "saveUserBalance");
    }

    public final function saveUserBalance($userId) {
        if (current_user_can('edit_user', $userId)) {
            $mapping = $this->getAustralianBankAccountDetailsMapping();

            foreach ($mapping as $key => $caption) {
                if (isset($_POST[$key])) {
                    update_user_meta($userId, "balance_" . $key, $_POST[$key]);
                }
            }
        }
    }

    public final function embedBalanceEditor(\WP_User $user) {
        if (!current_user_can('manage_options')) {
            return;
        }

        echo "<h3>" . __("Bank Account") . "</h3><table class=\"form-table\">";

        $mapping = $this->getAustralianBankAccountDetailsMapping();

        foreach ($mapping as $key => $caption) {
            $val = get_user_meta($user->ID, "balance_" . $key, true);
            echo "<tr><th><label for=\"{$key}\">{$caption}</label></th><td><input name=\"{$key}\" id=\"{$key}\" value=\"{$val}\"></td></tr>";
        }

        echo "</table>";

    }

    protected function registerOptions()
    {
        $this->registerOption("withdrawal_email", new TextOption($this, "withdrawal_email", "Withdrawal Email"));
        $this->registerOption("withdrawal_page", new PageSelector($this, "withdrawal_page", "Withdrawal Page"));
    }

    public final function getUserBalance($userId) {
        $balance = get_user_meta($userId, "seller_balance", true);
        $balance = empty($balance)?0:$balance;

        return $balance;
    }

    public final function setUserBalance($userId, $value) {
        update_user_meta($userId, "seller_balance", $value);
    }

    public final function addBalance($userId, $amount) {
        $this->setUserBalance($userId, $this->getUserBalance($userId) + $amount);
    }

    public final function getAustralianBankAccountDetailsMapping() {
        return array(
            "bank_name" => "Name of Bank or Financial Institution",
            "branch_name" => "Bank or Financial Institution Branch",
            "bsb" => "Branch number (BSB)",
            "account_number" => "Account number",
            "holder" => "Account holder"
        );
    }

    public final function requestWithdrawal($userId, $amount) {
        $userData = get_userdata($userId);
        $userName = implode(" ", array($userData->first_name, $userData->last_name));
        $userAmount = $this->getUserBalance($userId);

        if ($amount > $userAmount) {
            return false;
        }

        $currency = get_woocommerce_currency_symbol();

        $message = "<h1>Hello!</h1><br/><p>User <span>{$userName}</span> is asking you to withdraw {$amount} {$currency} to his bank account:</p>";
        $message .= "<table>";

        $mapping = $this->getAustralianBankAccountDetailsMapping();

        foreach ($mapping as $key => $caption) {
            $message .= "<tr><th>{$caption}</th><td>";

            $message .= get_user_meta($userId, "balance_" . $key, true);

            $message .= "</td></tr>";
        }

        $message .= "</table>";

        wp_mail($this->getOption("withdrawal_email")->getValue(), "Withdrawal Request for " . $userName, $message);

        $this->addBalance($userId, -$amount);

        return true;
    }
}