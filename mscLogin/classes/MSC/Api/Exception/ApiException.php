<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 19.06.16
 * Time: 23:24
 */

namespace MSC\Api\Exception;


class ApiException extends \Exception
{
    private $result;

    /**
     * ApiException constructor.
     */
    public function __construct($result)
    {
        parent::__construct("API method has raised an exception: " . $result['message']);
        $this->result = $result;
    }

    public function getMessageWithResult() {
        return $this->getMessage() . ". Result: " . print_r($this->result, true);
    }
}