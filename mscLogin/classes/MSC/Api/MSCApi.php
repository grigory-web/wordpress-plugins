<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 18.06.16
 * Time: 18:11
 */

namespace MSC\Api;

use MSC\Api\Exception\ApiException;

abstract class MSCUser extends \WP_User {
    protected $api;
    protected $mscId;

    /**
     * MSCCustomerUser constructor.
     */
    public function __construct(MSCApi $api, $id = 0, $name = '', $blog_id = '', $mscId)
    {
        parent::__construct($id, $name, $blog_id);
        $this->api = $api;
        $this->mscId = $mscId;
    }

    /**
     * @return mixed
     */
    public function getMscId()
    {
        return $this->mscId;
    }

    public abstract function getExtendedInfo();
    public abstract function getBalance();
    public function getEditProfileLink() {
        return $this->getExtendedInfo()['edit_url'];
    }
}

class MSCCustomerUser extends MSCUser {
    /**
     * MSCCustomerUser constructor.
     */
    public function __construct(MSCApi $api, $id, $name, $blog_id)
    {
        parent::__construct($api, $id, $name, $blog_id, $api->getMscUserId($id, "customer"));
    }

    public function getExtendedInfo()
    {
        return $this->api->getExtendedUserInfo($this->mscId, "customer");
    }

    public function getBalance()
    {
        return $this->api->getUserBalance($this->mscId, "customer");
    }

    public function getPaymentMethods() {
        return $this->api->getPaymentMethods($this->mscId);
    }
}


class MSCTuckshopUser extends MSCUser {
    /**
     * MSCCustomerUser constructor.
     */
    public function __construct(MSCApi $api, $id, $name, $blog_id)
    {
        parent::__construct($api, $id, $name, $blog_id, $api->getMscUserId($id, "tuckshop_user"));
    }

    public function getExtendedInfo()
    {
        return $this->api->getExtendedUserInfo($this->mscId, "tuckshop_user");
    }

    public function getBalance()
    {
        return $this->api->getUserBalance($this->mscId, "tuckshop_user");
    }
}

class MSCApi
{
    private $mscApiUrl;
    private $mscApiKey;

    private static $methodsMapping = array(
        "info" => array(
            "customer" => "api/customer/getCustomer",
            "tuckshop_user" => "api/tuckshop_user/getUser"
        ),
        "balance" => array(
            "customer" => "api/customer/getBalance",
            "tuckshop_user" => "api/tuckshop/getBalance"
        )
    );

    public function __construct()
    {
        global $mscPluginEntry;

        $this->mscApiKey = $mscPluginEntry->getFragment("msc_general")->getOption("msc_api_key")->getValue();
        $this->mscApiUrl = $mscPluginEntry->getFragment("msc_general")->getOption("msc_api_url")->getValue();
    }

    private function mscApiCall($method, $sessionToken, array $data = array()) {
        if (!empty($sessionToken))
            $data['session_token'] = $sessionToken;

        $data['api_key'] = $this->mscApiKey;

        $curlResource = curl_init();
        curl_setopt($curlResource, CURLOPT_URL, $this->mscApiUrl . "index.php?route=" . $method);
        curl_setopt($curlResource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlResource, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curlResource, CURLOPT_POST, true);

        //TODO For staging only!
        curl_setopt($curlResource, CURLOPT_USERPWD, "myschool:develop@#1");

        $result = curl_exec($curlResource);
        curl_close($curlResource);

        return json_decode($result, true);
    }

    public function getCurrentUserWrapped() {
        return $this->wrapUser(get_current_user_id(), $this->getCurrentMscUserRole());
    }

    public function wrapUser($userId, $role) {
        if ($role == "customer") {
            return new MSCCustomerUser($this, $userId, '', '');
        } else if ($role == "tuckshop_user") {
            return new MSCTuckshopUser($this, $userId, '', '');
        }

        throw new \InvalidArgumentException("Invalid role for user " . $userId . ": " . $role);
    }

    public function getCurrentMscUserRole() {
        if (isset($_SESSION['msc_role']))
            return $_SESSION['msc_role'];
        else
            return false;
    }

    public function getMscUserId($userId, $role) {
        if ($role == "customer") {
            $field = "msc_customer_id";
        } else if ($role == "tuckshop_user") {
            $field = "msc_tuckshop_id";
        } else {
            throw new \InvalidArgumentException("Invalid role for user " . $userId . ": " . $role);
        }

        return get_user_meta($userId, $field, true);
    }

    public function getUserBalance($userId, $role) {
        if ($role == "customer") {
            $params = array("id" => $userId);
        } else if ($role == "tuckshop_user") {
            $params = array("tuckshop_id" => $userId);
        } else {
            throw new \InvalidArgumentException("Invalid role for user " . $userId . ": " . $role);
        }

        $result = $this->mscApiCall(self::$methodsMapping['balance'][$role], '', $params);

        if ($result['status'] == "success") {
            if (!empty($result['data'][0])) {
                return $result['data'][0]['amount'];
            }
        }

        throw new ApiException($result);
    }

    public function getGeneralUserInfo($sessionToken) {
        $result = $this->mscApiCall("api/user/getUserId", $sessionToken);

        if ($result['status'] == "success") {
            if (!empty($result['data'][0])) {
                return $result['data'][0];
            }
        }

        throw new ApiException($result);
    }

    public function getExtendedUserInfo($userId, $role) {
        $result = $this->mscApiCall(self::$methodsMapping['info'][$role], "", array("id" => $userId));

        if ($result['status'] == "success") {
            if (!empty($result['data'][0])) {
                return $result['data'][0];
            }
        }

        throw new ApiException($result);
    }

    public function getUserByEmail($email) {
        $result = $this->mscApiCall("api/user/checkEmail", "", array("email" => $email));

        if ($result['status'] == "success") {
            if (count($result['data']) > 0) {
                return $result['data'];
            } else {
                return false;
            }
        }

        throw new ApiException($result);
    }

    public function getPaymentMethods($userId) {
        $result = $this->mscApiCall("api/customer/getPaymentMethods", "", array("id" => $userId));

        if ($result['status'] == "success") {
            if (count($result['data']) > 0) {
                return $result['data'];
            } else {
                return false;
            }
        }

        throw new ApiException($result);
    }

    public function addOrder($userId, $products, $fee) {
        $params = array("id" => is_array($userId)?0:$userId, "products" => $products, "fee" => $fee);

        if (is_array($userId)) {
            $params = array_merge($params, $userId);
        }

        //throw new \Exception(print_r($params, true));

        $result = $this->mscApiCall("api/order/addOrder", "", $params);

        if ($result['status'] == "success") {
            if (!empty($result['data'][0])) {
                return $result['data'][0]['order_id'];
            }
        }

        throw new ApiException($result);
    }

    public function payOrder($userId, $orderId, $amount, $tuckshops, $paymentDetails) {
        $params = array(
            "id" => $userId,
            "order_id" => $orderId,
            "amount" => $amount,
            "payment" => $paymentDetails
        );

        if (count($tuckshops) > 0) {
            $params['tuckshops'] = $tuckshops;
        }

        //throw new \Exception(print_r($params, true));

        $result = $this->mscApiCall("api/order/payOrder", "", $params);

        if ($result['status'] == "success") {
            return true;
        } else {
            throw new ApiException($result);
        }
    }


    public function confirmOrder($orderId, $tuckshops, $userId = 0) {
        $params = array(
            "id" => $userId,
            "order_id" => $orderId,
            "tuckshops" => $tuckshops
        );

        //throw new \Exception(print_r($params, true));

        $result = $this->mscApiCall("api/order/confirmOrder", "", $params);

        if ($result['status'] == "success") {
            return true;
        } else {
            throw new ApiException($result);
        }
    }

}