<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 11.04.16
 * Time: 16:05
 */

namespace MSC\Plugin;


use Core\Plugin\Options\CheckboxOption;
use Core\Plugin\Options\PageSelector;
use Core\Plugin\Options\TextOption;
use Core\Plugin\PluginFragment;
use MSC\Api\MSCApi;

class Woocommerce extends PluginFragment
{
    /**
     * General constructor.
     */
    public function __construct(\MSCLogin $plugin)
    {
        parent::__construct($plugin, "msc_woocommerce", "Woocommerce");

        if ($this->isEnabled()) {
            $this->attachActionListener("pluginsLoaded", "initWooCommerceClass");
            $this->attachFilter("woocommerce_payment_gateways", "addPaymentGateway");
            $this->attachFilter("woocommerce_available_payment_gateways", "onDisableMSCGateway", 1);
            $this->attachActionListener("woocommerce_order_status_processing", "onPostPayment");
        }
    }

    public function getSellerFee() {
        return 0.33;
    }

    public function getCustomerFee() {
        return 0;
    }

    public function onPostPayment($orderId) {
        $order = wc_get_order($orderId);
        $orderAmount = $order->get_total();
        $mscApiInstance = new MSCApi();

        //Split an order
        $items = $order->get_items();

        $parts = array(0 => array());

        foreach ($items as $order_item_id => $order_item) {
            $productInfo = array(
                "unit_price" => $order_item['line_total'] / $order_item['qty'],
                "description" => $order_item['name'],
                "quantity" => $order_item['qty']
            );

            $eventId = get_post_meta($order_item['product_id'], "_tribe_wooticket_for_event", true);//TODO This is not general and should be moved to a separate plugin
            $authorId = get_post_field("post_author", $eventId);

            //Check if product's "author" is or is not tuckshop user
            $tuckshopId = $mscApiInstance->getMscUserId($authorId, "tuckshop_user");


            if ($tuckshopId == "") {
                $productInfo['tuckshop_user_id'] = 0;
                $productInfo['oe_user_id'] = $authorId;
                $parts[0][] = $productInfo;
            } else {
                $productInfo['tuckshop_user_id'] = $tuckshopId;
                if (!array_key_exists($tuckshopId, $parts)) {
                    $parts[$tuckshopId] = array();
                }

                $parts[$tuckshopId][] = $productInfo;
            };
        }
        //Reorganize parts
        $addOrderParts = array();
        $payOrderParts = array();
        $oeOrderParts = array();

        foreach ($parts as $tuckshop_user_id => $items) {
            $addOrderParts = array_merge($addOrderParts, $items);

            //Calculate amount for each tuckshop_user_id
            $amount = 0.0;
            foreach ($items as $item) {
                if ($item['unit_price'] > 0.0) {
                    $amount += ($item['unit_price'] - $this->getSellerFee()) * $item['quantity'];
                }
            }

            $payOrderParts[] = array(
                "tuckshop_user_id" => $tuckshop_user_id,
                "amount" => $amount
            );

            if ($tuckshop_user_id == 0) {
                foreach ($items as $item) {
                    if (!array_key_exists($item['oe_user_id'], $oeOrderParts))
                        $oeOrderParts[$item['oe_user_id']] = 0.0;

                    $oeOrderParts[$item['oe_user_id']] += ($item['unit_price'] - $this->getSellerFee()) * $item['quantity'];
                }
            }
        }

        unset($payOrderParts[0]);

        if (count($payOrderParts) > 0 && !current_user_can("msc_user")) {
            //Place an order via API
            try {
                $userData = get_userdata(get_current_user_id());

                $innerOrderId = $mscApiInstance->addOrder(array(
                    "firstname" => $userData->first_name,
                    "lastname" => $userData->last_name,
                    "email" => $userData->user_email
                ), $addOrderParts, $orderAmount * $this->getCustomerFee() / 100.0);

                $mscApiInstance->confirmOrder($innerOrderId, $payOrderParts);
            } catch (\Exception $e) {
                die($e->getMessage());
            }
        }

        if (count($oeOrderParts) > 0) {
            foreach ($oeOrderParts as $userId => $amount) {
                $balance = get_user_meta($userId, "seller_balance", true);
                $balance = empty($balance)?0:$balance;

                update_user_meta($userId, "seller_balance", $balance + $amount);
            }
        }
    }

    public function onDisableMSCGateway($gateways) {
        if (!current_user_can("msc_user")) {
            global $woocommerce;
            unset($gateways["msc_credits_gateway"]);
            unset($gateways["msc_cc_gateway"]);
        }
        return $gateways;
    }

    public function initWooCommerceClass() {
        require_once dirname(__FILE__) . "/PaymentGateway/MSCPaymentGateway.php";
        require_once dirname(__FILE__) . "/PaymentGateway/MSCCreditsPaymentGateway.php";
        require_once dirname(__FILE__) . "/PaymentGateway/MSCCCPaymentGateway.php";
    }

    public function addPaymentGateway($gateways) {
        $gateways[] = "MSC\\Plugin\\PaymentGateway\\MSCCreditsPaymentGateway";
        $gateways[] = "MSC\\Plugin\\PaymentGateway\\MSCCCPaymentGateway";
        return $gateways;
    }

    protected function registerOptions()
    {
    }


}