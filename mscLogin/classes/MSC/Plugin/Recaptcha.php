<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 11.04.16
 * Time: 16:05
 */

namespace MSC\Plugin;


use Core\Plugin\Options\CheckboxOption;
use Core\Plugin\Options\PageSelector;
use Core\Plugin\Options\TextOption;
use Core\Plugin\PluginFragment;

class Recaptcha extends PluginFragment
{
    /**
     * General constructor.
     */
    public function __construct(\MSCLogin $plugin)
    {
        parent::__construct($plugin, "msc_recaptcha", "Recaptcha");

        if ($this->isEnabled()) {
            $this->attachFilter("registration_errors", "onCheckRecaptcha", 1, 3);
        }
    }


    private function validateRecaptcha($recaptchaResponse) {
        $curlResource = curl_init();
        curl_setopt($curlResource, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($curlResource, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlResource, CURLOPT_POSTFIELDS, http_build_query(array(
            "secret" => $this->getOption("msc_recaptcha_secret")->getValue(),
            "response" => $recaptchaResponse
        )));
        curl_setopt($curlResource, CURLOPT_POST, true);

        $result = curl_exec($curlResource);
        curl_close($curlResource);

        $result = json_decode($result, true);

        return $result['success'];
    }

    public function onCheckRecaptcha($errors, $userLogin, $userEmail) {
        /**
         * @var \WP_Error $errors
         */

        if (!isset($_POST['g-recaptcha-response'])) {
            $errors->add("invalid_captcha", "");
        } else {
            if (!$this->validateRecaptcha($_POST['g-recaptcha-response'])) {
                $errors->add("invalid_captcha", "");
            }
        }

        return $errors;
    }

    protected function registerOptions()
    {
        $this->registerOption("msc_recaptcha_secret", new TextOption($this, "msc_recaptcha_secret", "Recaptcha secret", "<SECRET>", ""));
    }


}