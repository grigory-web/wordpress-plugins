<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 12.05.16
 * Time: 21:04
 */

namespace MSC\Plugin;


use Core\Plugin\Options\PageSelector;
use Core\Plugin\PluginFragment;

class Redirect extends PluginFragment
{
    /**
     * Redirect constructor.
     */
    public function __construct(\MSCLogin $plugin)
    {
        parent::__construct($plugin, "msc_templates", "Custom page templates");

        if ($this->isEnabled()) {
            $this->attachActionListener("add_meta_boxes", "onAddMetaBox");
            $this->attachActionListener("template_redirect", "onPossibleRedirect");
            $this->attachFilter("template_include", "onChangeTemplate");
            $this->attachFilter("the_content", "onChangeContent");
            $this->attachActionListener("save_post", "onSavePost", 10, 3);
            $this->attachActionListener("init", "onSetRefererCookie");
        }
    }

    public function onPossibleRedirect() {
        global $post;


        if (isset($post->ID)) {
            $currentRedirects = get_post_meta($post->ID, "msc_redirects", true);
            $currentUserId = get_current_user_id();

            if (empty($currentRedirects)) {
                $currentRedirects = array(
                    "msc" => "",
                    "mse" => ""
                );
            }

            if ($currentUserId != 0) {
                if (current_user_can("msc_user")) {
                    if ($currentRedirects["msc"] != "") {
                        wp_redirect($currentRedirects["msc"]);
                        exit();
                    }
                } else {
                    if ($currentRedirects["mse"] != "") {
                        wp_redirect($currentRedirects["mse"]);
                        exit();
                    }
                }
            }
        }
    }

    public function onSetRefererCookie() {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $ref = $_SERVER['HTTP_REFERER'];

            if (strpos($ref, "myschoolconnect.com.au") != false) {
                setcookie("is_msc_ref", true);
            }
        }
    }

    public function onSavePost($postId, $post, $update) {
        global $wpdb;

        // Check post type
        if ($post->post_type != 'page') {
            return;
        }

        // Admin area only
        if (!is_admin()) {
            return;
        }

        // Check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }

        //check post revision
        if (wp_is_post_revision($postId)) {
            return;
        }


        if (isset($_POST['msc_alternative_msc']) && isset($_POST['msc_alternative_mse'])) {
            $templates = array(
                "msc" => $_POST['msc_alternative_msc'],
                "mse" => $_POST['msc_alternative_mse']
            );

            update_post_meta($postId, "msc_alternatives", $templates);
        }

        if (isset($_POST['msc_redirect_msc']) && isset($_POST['msc_redirect_mse'])) {
            $redirects = array(
                "msc" => $_POST['msc_redirect_msc'],
                "mse" => $_POST['msc_redirect_mse']
            );

            update_post_meta($postId, "msc_redirects", $redirects);
        }
    }


    public function onRenderMetaBox() {
        global $post;

        $pages = get_posts(array("post_type" => "page", "numberposts" => -1));
        array_unshift($pages, (object)array("ID" => -1, "post_title" => "No alternative"));
        $templates["dummy"] = "Do not change the page template";
        $currentTemplates = get_post_meta($post->ID, "msc_alternatives", true);
        $currentRedirects = get_post_meta($post->ID, "msc_redirects", true);

        if (empty($currentTemplates)) {
            $currentTemplates = array(
                "msc" => -1,
                "mse" => -1
            );
        }

        if (empty($currentRedirects)) {
            $currentRedirects = array(
                "msc" => "",
                "mse" => ""
            );
        }

        $ways = array(
            "msc" => "MSC",
            "mse" => "MSE"
        );


        foreach ($ways as $way => $title) {
            echo "<label for='msc_alternative_" . $way . "'>Page on " . $title . " user</label>";
            echo "<select name='msc_alternative_" . $way . "' id='msc_alternative_" . $way . "'>";

            foreach ($pages as $page) {
                echo "<option value='" . $page->ID . "'";

                if ($currentTemplates[$way] == $page->ID) {
                    echo " selected";
                }

                echo ">" . $page->post_title . "</option>";
            }

            echo "</select>";
        }

        foreach ($ways as $way => $title) {
            echo "<label for='msc_redirect_" . $way . "'>Redirect on " . $title . " user</label>";
            echo "<input name='msc_redirect_" . $way . "' id='msc_redirect_" . $way . "' value='" . $currentRedirects[$way] . "'>";
        }
    }

    public function onAddMetaBox() {
        add_meta_box(
            "msc_template_metabox",      // $id
            'MSC Custom Page Template',          // $title
            array($this, "onRenderMetaBox"),  	   // $callback
            "page",                 // $pageС
            "side",                  // $context
            "default"                     // $priority
        );
    }

    private function isMSCUser($userId)
    {
        if (isset($_COOKIE['is_msc_ref'])) {
            return true;
        }

        return (get_user_meta($userId, "msc_customer_id", true) != "");
    }

    public function onChangeContent($content) {
        global $post;
        $newPost = $post;

        if (isset($post->ID)) {
            $currentAlternatives = get_post_meta($post->ID, "msc_alternatives", true);
            $currentUserId = get_current_user_id();

            if (empty($currentAlternatives)) {
                $currentAlternatives = array(
                    "msc" => -1,
                    "mse" => -1
                );
            }

            if ($currentUserId != 0) {
                if (current_user_can("msc_user")) {
                    if ($currentAlternatives["msc"] != -1) {
                        $newPost = get_post($currentAlternatives["msc"]);
                    }
                } else {
                    if ($currentAlternatives["mse"] != -1) {
                        $newPost = get_post($currentAlternatives["mse"]);
                    }
                }
            }

            return $newPost->post_content;
        }

        return $content;
    }

    public function onChangeTemplate($template) {
        global $post;


        if (isset($post->ID)) {
            $currentAlternatives = get_post_meta($post->ID, "msc_alternatives", true);
            $currentUserId = get_current_user_id();

            if (empty($currentAlternatives)) {
                $currentAlternatives = array(
                    "msc" => -1,
                    "mse" => -1
                );
            }

            if ($currentUserId != 0) {
                if (current_user_can("msc_user")) {
                    if ($currentAlternatives["msc"] != -1) {
                        $post = get_post($currentAlternatives["msc"]);
                    }
                } else {
                    if ($currentAlternatives["mse"] != -1) {
                        $post = get_post($currentAlternatives["mse"]);
                    }
                }
            }

            if (isset($_REQUEST['debug'])) {
                $isMsc = $this->isMSCUser($currentUserId);
                print_r($currentUserId);
                var_dump($isMsc);
                print_r($post);
                print_r($currentAlternatives);
                die(get_page_template());
            }

            return get_page_template();
        }

        return $template;
    }

    protected function registerOptions()
    {
    }


}