<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 19.06.16
 * Time: 12:29
 */

namespace MSC\Plugin\PaymentGateway;


use MSC\Api\MSCApi;

class MSCCCPaymentGateway extends MSCPaymentGateway
{
    /**
     * MSCCreditsPaymentGateway constructor.
     */
    public function __construct()
    {
        parent::__construct("msc_cc_gateway", "Credit Card", "Payment using credit card");

        try {
            $paymentMethods = (new MSCApi())->getCurrentUserWrapped()->getPaymentMethods();

            //Search for a CC payment method
            foreach ($paymentMethods as $method) {
                if ($method['code'] == "cc") {
                    if (!empty($method['method_data']['card_number'])) {
                        $this->title = "Credit Card: " . $method['method_data']['card_number'];
                        $this->method_title = "Credit Card: " . $method['method_data']['card_number'];
                    } else {
                        $this->has_fields = true;
                    }

                    break;
                }
            }
        } catch (\Exception $e) {

        }
    }

    public function payment_fields()
    {
        $this->credit_card_form();
    }

    public function getCustomerFee()
    {
        return 1.3;
    }

    public function isPaymentPossible($userId, $amount)
    {
        return true;
    }

    public function payOrder($amount)
    {
        if (isset($_POST['msc_cc_gateway-card-number']) && isset($_POST['msc_cc_gateway-card-expiry']) && isset($_POST['msc_cc_gateway-card-cvc'])) {
            $expiry = explode(" / ", $_POST['msc_cc_gateway-card-expiry']);
            $expiryMonth = $expiry[0];
            $expiryYear = \DateTime::createFromFormat('y', $expiry[1])->format('Y');

            return array(
                "code" => "cc",
                "data" => array(
                    "saved_card" => 0,
                    "card_number" => $_POST['msc_cc_gateway-card-number'],
                    "expiry_month" => $expiryMonth,
                    "expiry_year" => $expiryYear,
                    "ccv" => $_POST['msc_cc_gateway-card-cvc']
                )
            );
        } else {
            return array(
                "code" => "cc",
                "data" => array(
                    "saved_card" => 1,
                    "card_number" => 0,
                    "expiry_month" => 0,
                    "expiry_year" => 0,
                    "ccv" => 0
                )
            );
        }
    }
}