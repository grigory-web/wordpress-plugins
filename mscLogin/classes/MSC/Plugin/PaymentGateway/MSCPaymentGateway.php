<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 03.06.16
 * Time: 11:17
 */

namespace MSC\Plugin\PaymentGateway;

use MSC\Api\MSCApi;

require_once dirname(__FILE__) . "/../../../../msc-login.php";

abstract class MSCPaymentGateway extends \WC_Payment_Gateway
{
    public function __construct($id = "msc_payment_gateway", $title = "MSC Payment Gateway", $description = "")
    {
        $this->id = $id;
        $this->icon = "";
        $this->has_fields = false;
        $this->title = $title;
        $this->method_title = $title;
        $this->method_description = $description;

        $this->init_form_fields();
        $this->init_settings();

        $this->enabled = $this->get_option("enabled", false);

        add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
    }

    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title' => __( 'Enable/Disable', 'woocommerce' ),
                'type' => 'checkbox',
                'label' => __( 'Enable MSC API Payment', 'woocommerce' ),
                'default' => 'yes'
            )
        );
    }

    public abstract function getCustomerFee();
    public function getSellerFee() {
        return 0.33;
    }
    public abstract function isPaymentPossible($userId, $amount);
    public abstract function payOrder($amount);



    public function process_payment( $order_id ) {
        //Check if the user is a MSC customer
        if (!current_user_can("msc_user")) {
            wc_add_notice( __('Payment error:', 'woothemes') . "You are not allowed to use MSC payment!", 'error' );
            return;
        }

        global $woocommerce;
        $order = new \WC_Order( $order_id );

        $orderAmount = $order->get_total();
        //Ask the API about user's balance
        $mscApiInstance = new MSCApi();
        try {
            $currentUser = $mscApiInstance->getCurrentUserWrapped();
        } catch (\Exception $e) {
            wc_add_notice(__('Payment error:', 'woothemes') . $e->getMessage(), 'error');
            return;
        }

        try {
        if (!($error = $this->isPaymentPossible($currentUser->getMscId(), $orderAmount))) {
            wc_add_notice( __('Payment error:', 'woothemes') . $error, 'error' );
            return;
        }
        } catch (\Exception $e) {
            wc_add_notice(__('Payment error:', 'woothemes') . $e->getMessage(), 'error');
            return;
        }

        //Split an order
        $items = $order->get_items();

        $parts = array(0 => array());

        foreach ($items as $order_item_id => $order_item) {
            $productInfo = array(
                "unit_price" => $order_item['line_total'] / $order_item['qty'],
                "description" => $order_item['name'],
                "quantity" => $order_item['qty']
            );

            $eventId = get_post_meta($order_item['product_id'], "_tribe_wooticket_for_event", true);//TODO This is not general and should be moved to a separate plugin
            $authorId = get_post_field("post_author", $eventId);

            //Check if product's "author" is or is not tuckshop user
            $tuckshopId = $mscApiInstance->getMscUserId($authorId, "tuckshop_user");

            if ($tuckshopId == "") {
                $productInfo['tuckshop_user_id'] = 0;
                $parts[0][] = $productInfo;
            } else {
                $productInfo['tuckshop_user_id'] = $tuckshopId;
                if (!array_key_exists($authorId, $parts)) {
                    $parts[$tuckshopId] = array();
                }

                $parts[$tuckshopId][] = $productInfo;
            };
        }

        //Reorganize parts
        $addOrderParts = array();
        $payOrderParts = array();

        foreach ($parts as $tuckshop_user_id => $items) {
            $addOrderParts = array_merge($addOrderParts, $items);

            //Calculate amount for each tuckshop_user_id
            $amount = 0.0;
            foreach ($items as $item) {
                if ($item['unit_price'] > 0.0) {
                    $amount += ($item['unit_price'] - $this->getSellerFee()) * $item['quantity'];
                }
            }

            $payOrderParts[] = array(
                "tuckshop_user_id" => $tuckshop_user_id,
                "amount" => $amount
            );
        }

        unset($payOrderParts[0]);

        //wc_add_notice(__('Payment error:', 'woothemes') . print_r($addOrderParts, true), 'error');
        //wc_add_notice(__('Payment error:', 'woothemes') . print_r($payOrderParts, true), 'error');


        //Place an order via API
        try {
            $innerOrderId = $mscApiInstance->addOrder($currentUser->getMSCId(), $addOrderParts, $orderAmount * $this->getCustomerFee() / 100.0);

            if ($orderAmount == 0.0) {
                $mscApiInstance->confirmOrder($innerOrderId, $payOrderParts, $currentUser->getMscId());
                //Complete the payment in WooCommerce
                $order->payment_complete();

                // Return thankyou redirect
                return array(
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                );
            } else {
                if (($paymentDetails = $this->payOrder($orderAmount)) !== false) {
                    if ($mscApiInstance->payOrder($currentUser->getMscId(), $innerOrderId, $orderAmount * (1 + $this->getCustomerFee() / 100.0), $payOrderParts, $paymentDetails)) {
                        //Complete the payment in WooCommerce
                        $order->payment_complete();

                        // Return thankyou redirect
                        return array(
                            'result' => 'success',
                            'redirect' => $this->get_return_url($order)
                        );
                    }
                }
            }
        } catch (\Exception $e) {
            wc_add_notice(__('Payment error:', 'woothemes') . $e->getMessage(), 'error');
            return;
        }


        wc_add_notice( __('Payment error:', 'woothemes') . "Exception occured!", 'error' );
        return;
    }


}