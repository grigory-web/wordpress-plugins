<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 19.06.16
 * Time: 12:29
 */

namespace MSC\Plugin\PaymentGateway;


use MSC\Api\MSCApi;

class MSCCreditsPaymentGateway extends MSCPaymentGateway
{
    /**
     * MSCCreditsPaymentGateway constructor.
     */
    public function __construct()
    {
        parent::__construct("msc_credits_gateway", "MSC Credits", "Payment using MSC credits");

        try {
            $methods = (new MSCApi())->getCurrentUserWrapped()->getPaymentMethods();

            //Search for a credit payment method
            foreach ($methods as $method) {
                if ($method['code'] == "credit") {
                    $this->title = "MSC Credits (" . $method['method_data']['amount'] . " available)";
                    $this->method_title = "MSC Credits (" . $method['method_data']['amount'] . " available)";
                    break;
                }
            }
        } catch (\Exception $e) {

        }
    }

    public function getCustomerFee()
    {
        return 0;
    }

    public function isPaymentPossible($userId, $amount)
    {
        $mscApiInstance = new MSCApi();
        return ($mscApiInstance->getUserBalance($userId, "customer") > $amount)?true:"Not enough credits!";
    }

    public function payOrder($amount)
    {
        return array(
            "code" => "credit",
            "data" => array()
        );
    }
}