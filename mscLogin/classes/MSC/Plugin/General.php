<?php
/**
 * Created by PhpStorm.
 * User: madmax
 * Date: 11.04.16
 * Time: 16:05
 */

namespace MSC\Plugin;


use Core\Plugin\Options\CheckboxOption;
use Core\Plugin\Options\PageSelector;
use Core\Plugin\Options\TextOption;
use Core\Plugin\PluginFragment;
use MSC\Api\MSCApi;

class General extends PluginFragment
{
    /**
     * General constructor.
     */
    public function __construct(\MSCLogin $plugin)
    {
        parent::__construct($plugin, "msc_general", "General", false);

        $this->attachActionListener("login_form_login", "onLoginAttempt", 1);//Set the highest priority
        $this->attachActionListener("login_form_logout", "onLogoutAttempt", 1);//Set the highest priority
        $this->attachFilter("authenticate", "onAuthorize", 30, 3);//Set the highest priority
        $this->attachFilter("logout_url", "onGenerateLogoutUrl", 99, 2);

        $this->attachFilter("registration_errors", "onCheckRegistrationForMSCUser", 1, 3);

        $this->addShortcode("mscLoginLink", "getMSCLoginLink");//Possibly unused

        //Sessions
        $this->attachActionListener("init", "onStartSession", 1);
        $this->attachActionListener("wp_logout", "onEndSession");
        $this->attachActionListener("wp_login", "onEndSession");

        $this->attachFilter("allow_password_reset", "onDisallowRP", 10, 2);

        //Debug
        $this->addShortcode("debug-current-user", "onDebugCurrentUser");
    }

    public function onStartSession() {
        if (!session_id())
            session_start();
    }

    public function onEndSession() {
        session_destroy();
    }

    public function onDebugCurrentUser($args = array()) {
        $isLoggedIn = is_user_logged_in();
        $isMsc = current_user_can("msc_user");
        $mscUserRole = $this->getCurrentMscUserRole();
        $mscUserId = $this->getMscUserId(get_current_user_id());

        return implode("", array_map(function($val) {
            return "<p><span>" . $val[0] . "</span><span style='font-weight: bold;'>" . var_export($val[1], true) . "</span></p>";
        }, array(
            array("Is user logged in: ", $isLoggedIn),
            array("Is user msc related: ", $isMsc),
            array("MSC Role: ", $mscUserRole),
            array("MSC Id: ", $mscUserId)
        )));
    }

    public function onGenerateLogoutUrl($logout_url, $redirect) {
        if (current_user_can("msc_user")) {
            return $this->getOption("msc_api_url")->getValue() . "index.php?route=account/logout";
        } else {
            return $logout_url;
        }
    }

    private function generateLoginUrl($base) {
        //Workaround on a strange site-url filter
        /*$path = "wp-login.php";
        $scheme = null;
        $url = get_option( 'siteurl' );
        $url = set_url_scheme( $url, $scheme );

        if ( $path && is_string( $path ) )
            $url .= '/' . ltrim( $path, '/' );*/
        $url = wp_login_url();

        $base = add_query_arg("api_id", $this->getOption("msc_api_id")->getValue(), $base);
        $base = add_query_arg("return_url", $url, $base);

        return $base;
    }

    public function onDisallowRP($result, $userId) {
        if (user_can($userId, "msc_user")) {
            return new \WP_Error("msc_not_allowed", "Invalid username or e-mail");
        }

        return $result;
    }

    public function getMSCLoginLink() {
        //Workaround on a strange site-url filter
        $path = "wp-login.php";
        $scheme = null;
        $url = get_option( 'siteurl' );
        $url = set_url_scheme( $url, $scheme );

        if ( $path && is_string( $path ) )
            $url .= '/' . ltrim( $path, '/' );

        return "<p><a href=\"" . $this->getOption("msc_api_url")->getValue() . "login?api_id=" . $this->getOption("msc_api_id")->getValue() . "&return_url=" . $url . "\">Login with MSC</a></p>";
    }

    public function onLoginAttempt() {
        if (isset($_REQUEST['session_token'])) {
            //We've got the session token
            $mscSessionToken = $_REQUEST['session_token'];

            //Let's fetch a user id
            $mscApiInstance = new MSCApi();
            $data = $mscApiInstance->getGeneralUserInfo($_REQUEST['session_token']);
            $userId = $data['id'];
            $role = $data['role'];

            //First attempt: Find a user by user id and role
            $usersArray = get_users(array(
                "role" => "msc_user",
                "meta_query" => array(
                    "relation" => "OR",
                    array(
                        "key" => "msc_customer_id",
                        "value" => $userId,
                        "compare" => "="
                    ),
                    array(
                        "key" => "msc_tuckshop_id",
                        "value" => $userId,
                        "compare" => "="
                    )
                ),
                "number" => 1
            ));

            if (count($usersArray) > 0) {
                //User found!
                $userToSet = $usersArray[0];
            } else {
                //User not found!
                //Second attempt: Find a user by email
                //First fetch more info
                $userInfo = $mscApiInstance->getExtendedUserInfo($userId, $role);

                //Try to find an existing one by email
                $emailUser = get_user_by("email", $userInfo['email']);

                if ($emailUser == false) {
                    //Finally user doesn't exist. Create one.
                    $emailUser = new \WP_User(wp_create_user(
                        $userInfo['email'],
                        wp_generate_password(),
                        $userInfo['email']
                    ));
                }

                wp_update_user(array(
                    "ID" => $emailUser->ID,
                    "first_name" => $userInfo['firstname'],
                    "last_name" => $userInfo['lastname']
                ));

                if ($role == "customer") {
                    update_user_meta($emailUser->ID, "msc_customer_id", $userId);
                } else if ($role == "tuckshop_user") {
                    update_user_meta($emailUser->ID, "msc_tuckshop_id", $userId);
                }

                $emailUser->set_role("msc_user");
                $userToSet = $emailUser;
            }

            if (isset($userToSet)) {
                //Authorize the user
                wp_set_current_user($userToSet->ID);
                wp_set_auth_cookie($userToSet->ID);
                $_SESSION['msc_role'] = $role;
                return;
            }
        }
    }

    public function onAuthorize($user, $username, $password) {
        $users = (new MSCApi())->getUserByEmail($username);

        if ($users !== false) {
            return new \WP_Error(rawurlencode("msc_auth_error:" . implode("~", array_map(function ($el) {
                    return $el['role'] . "^" . $this->generateLoginUrl($el['login_url']);
                }, $users))), "MSC users have to login via MSC!");
        }

        return $user;
    }

    public function onLogoutAttempt() {
        if (isset($_REQUEST['session_token'])) {
            //We've got the session token
            $mscSessionToken = $_REQUEST['session_token'];

            //Let's fetch a customer id
            $mscApiInstance = new MSCApi();
            $userInfo = $mscApiInstance->getGeneralUserInfo($mscSessionToken);

            $userId = $userInfo['id'];
            $innerUserId = $mscApiInstance->getMscUserId(get_current_user_id(), $mscApiInstance->getCurrentMscUserRole());

            if ($userId == $innerUserId) {
                //Logout the user
                session_destroy();
                wp_logout();
                return;
            }
        }
    }

    public function onCheckRegistrationForMSCUser($errors, $userLogin, $userEmail) {
        /**
         * @var \WP_Error $errors
         */

            $result = (new MSCApi())->getUserByEmail($userEmail);

            if ($result !== false) {
                if (count($result) > 0) {

                    $errors->add("msc_presented:" . rawurlencode($result[0]['login_url']), "");
                }
            }

        return $errors;
    }


    protected function registerOptions()
    {
        $this->registerOption("msc_api_url", new TextOption($this, "msc_api_url", "MSC API URL", "http://staging.myschoolconnect.com.au/", "Please, provide a trailing slash"));
        $this->registerOption("msc_api_id", new TextOption($this, "msc_api_id", "MSC API Identifier", "<ID>"));
        $this->registerOption("msc_api_key", new TextOption($this, "msc_api_key", "MSC API Key", "<Key>"));
        $this->registerOption("msc_api_is_staging", new CheckboxOption($this, "msc_api_is_staging", "Using staging website?", true));
    }


}