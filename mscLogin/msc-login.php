<?php

/*
Plugin Name: MSC Login
Description: A plugin that enables MSC login feature
Version: 1.0
Author: Maxim Penkov
Author URI: http://upwork.com/freelancers/~0193ca6eb9e7bcd3bb
*/


spl_autoload_register(function($className) {
    $fileName = plugin_dir_path(__FILE__) . "../mmxPluginCore/classes/" . str_replace("\\", "/", $className) . ".php";
    if (file_exists($fileName)) {
        include_once $fileName;
    }
});

spl_autoload_register(function($className) {
    $fileName = plugin_dir_path(__FILE__) . "/classes/" . str_replace("\\", "/", $className) . ".php";
    if (file_exists($fileName)) {
        include_once $fileName;
    }
});

error_reporting(E_ALL);
ini_set('display_errors', 1);


class MSCLogin extends \Core\Plugin\PluginEntryPoint
{
    /**
     * MSCLogin constructor.
     */
    public function __construct()
    {
        parent::__construct("msc-login", "MSC Login");
    }

    protected function attachPluginFragments()
    {
        $this->attachPluginFragment("msc_general", new \MSC\Plugin\General($this));
        $this->attachPluginFragment("msc_redirect", new \MSC\Plugin\Redirect($this));
        $this->attachPluginFragment("msc_recaptcha", new \MSC\Plugin\Recaptcha($this));
        $this->attachPluginFragment("msc_woocommerce", new \MSC\Plugin\Woocommerce($this));
        $this->attachPluginFragment("balance", new \MSC\Balance($this));
    }

    public function obtainMscApiInstance() {
        return new \MSC\Api\MSCApi();
    }

    public function onActivation() {
        //Remove roles
        remove_role("msc_customer");
        remove_role("msc_seller");
        remove_role("msc_user");

        //Add the roles back
        add_role("msc_user", "MSC User", array(
            "read" => true
        ));
    }
};
$mscPluginEntry = new MSCLogin();

register_activation_hook(__FILE__, array($mscPluginEntry, "onActivation"));